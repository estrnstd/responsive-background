/*
 * This class leverages a <picture> element inside another element, but applies the current src of that
 * picture element as a background image
 *
 */
class responsiveBackground {

	constructor( selector, options ) {

		try {

			this.apply_background(selector, options);

			var self = this;

			window.addEventListener('resize', 
				function() {

					//
					// Using a debounce technique here with timer
					//
					let body_element = document.querySelector('body');
					let timer_name = 'data-responsive-bg-timer-' + selector.replace(/[^A-Za-z0-9-]/gi, '-');
					let timer = body_element.getAttribute(timer_name);

					if ( timer !== null ) {
						clearTimeout(timer);
					}

					body_element.setAttribute(timer_name, 
						setTimeout(
						  function () {

							self.apply_background(selector, options);
							body_element.setAttribute(timer_name, null);
						  }
						  , 100
						)
					);

				}
			);

		}
		catch( e ) {
			if ( typeof(console) != 'undefined' && typeof(console.log) != 'undefined' ) {
				console.log(e.message);
			}
		}

	}

	apply_background( selector, options ) {

		try {

			options = options || {};

			let element = document.querySelectorAll(selector);

			if ( element !== null ) {

				element.forEach(function(item) {
					let picture = item.querySelector('picture');

					if ( picture !== null ) {
						let image   = picture.querySelector('img');

						if ( image !== null ) {

							let class_name_loaded = options.class_name_loading || 'background-loaded';
							let src = image.currentSrc || image.src;
							let dest_element = null;

							if ( typeof(options.apply_to) != 'undefined' ) {
								dest_element = document.querySelector(options.apply_to);
							}
							else {
								dest_element = item;
							}

							dest_element.style.backgroundImage = 'url(' + src + ')';

							if ( !dest_element.classList.contains(class_name_loaded) ) {
								dest_element.classList.add(class_name_loaded);
							}

						}
					}
				});
			}
		}
		catch( e ) {
			throw e;
		}


	}

}

module.exports = responsiveBackground;
