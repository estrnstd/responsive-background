@Description: 
This script will read the src of the <img> in a <picture> that’s the child of the designated container and add it as the background image of the designated container.  On resize, the background image will update as per the img src.


@Use: 

1) Place the <picture> as a child of your designated container

<div class=“some-container”>
	<picture>
    		<source srcset="/images/landscape-1366x480.jpg" media="(min-width: 800px)" type="image/jpeg">
    		<source srcset="/images/landscape-800x331.jpg" media="(min-width: 0px)" type="image/jpeg">
    		<img src="/images/landscape-1366x480.jpg" typeof="foaf:Image">
  	</picture>
</div>

2) Initiate the script

let responiveBG = new responsiveBackground('.responsive-background-image');